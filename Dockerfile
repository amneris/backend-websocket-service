FROM node:5.11.1-slim

MAINTAINER abaenglish

ENV HOME=/www

# Bundle app source
ADD server $HOME/server/
ADD package.json $HOME/package.json
ADD forever $HOME/forever
ADD bin $HOME/bin

WORKDIR $HOME
RUN npm install forever -g && npm install --silent --production

EXPOSE 8080:8080

ENTRYPOINT ["/bin/bash", "bin/run.sh"]
