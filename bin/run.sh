#!/bin/bash

export RUNDIR="."

cd $RUNDIR

APP_NAME=$APP_NAME NEW_RELIC_LICENSE_KEY=$NEW_RELIC_LICENSE_KEY node_modules/forever/bin/forever start $RUNDIR/forever/production.json ${@:1}
node_modules/forever/bin/forever --f logs 0
