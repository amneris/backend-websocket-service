var gulp = require('gulp');
var ABAGitRelease = require('gulp-aba-git-release');

module.exports = function () {

  username = this.opts.username;
  password = this.opts.password;
  repositoryUrl = 'https://' + username + ':' + password + '@github.com/abaenglish/backend-websocket-service.git';

  ABAGitRelease({username: username, password: password, repositoryUrl: repositoryUrl})
};
