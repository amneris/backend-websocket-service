// script for building app
var gulp = require('gulp');
var argv = require('yargs').argv;


/* *************************************************************************/
/* ****** GULP TASKS *******************************************************/
/* *************************************************************************/

var dirGulpTasks = 'gulp-tasks';

gulp.task('release', function () {
  var username = argv.username;
  var password = argv.password;

  if (!username || !password) {
    console.log('Please specify github username and password');
    return false;
  }

  require('gulp-task-loader')({
    username: username,
    password: password,
    dir: dirGulpTasks
  });

});
