# Web Client (used for testing purposes only)

In this directory you'll find an example web-application that can be used to test the WebsocketsToMQ service.

## Running the app

From the project's root directory:

* Install project dependencies: ```npm install```

* Run an instance of a RabbitMQ message broker: ```docker run -p 4369:4369 -p 5671-5672:5671-5672 -p 15671-15672:15671-15672 -p 25672:25672 -d rabbitmq:3-management```

* Run an instance of a Redis db: ```docker run -p 6379:6379 -d redis```

* Start the service ```export ENVIRONMENT=local && node .```

* Then run the webapp ```node resources/client/webapp.js```

* Open the webapp at http://localhost:3090/

## Testing the app

User the form inputs, you can subscribe to the WebsocketsToMQ service selecting a transactionId and the message topic that you wish to subscribe to.


You can then send messages to the message broker via curl using RabbitMQ's http api:

```
curl -i -u guest:guest -H "Content-Type: application/json" -XPOST -d '{"properties":{"content_type":"application/json"},"routing_key":"CREATED","payload":"{\"transactionId\":\"1\",\"zuoraId\":\"123\"}","payload_encoding":"string"}' http://localhost:15672/api/exchanges/%2f/subscription-service/publish
```

If the transactionId and topic sent in the message's payload match the form parameters submitted in the web page, then the message sent via the curl command will appear in the web page.

More information about the WebsocketsToMQ service can be found in the project's root README.md
