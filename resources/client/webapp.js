'use strict';

/**
 * serves a static webpage for testing the app
 */

var app = require('express')();
var http = require('http').Server(app);

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});


http.listen(3090, function () {
    console.log('** WEB APP ** listening on port 3090');
});
