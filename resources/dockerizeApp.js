var argv = require('yargs').argv;

var nexusServer =  argv.nexusServer;
var projectName =  argv.projectName;

if (nexusServer && projectName) {
  var dockerPusher = require('docker-manager-nodejs');

  dockerPusher.buildTagPush(nexusServer, projectName);
}
else {
  throw(new Error("Missing params to create the image"))
}
