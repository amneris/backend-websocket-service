'use strict';

module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/health', server.loopback.status());

  //TODO: remove, only used for testing crashes
  /*
  router.get('/crash', function() {
    process.nextTick(function () {
      throw new Error;
    });
  });*/

  server.use(router);
};
