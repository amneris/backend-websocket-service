var logger = require("../logger/logger");
/**
 * function to retrieve the application's config from Spring Cloud Server
 * @param app
 * @param callback
 */
module.exports = function (app) {
  try {

    var request = require('sync-request');
    var configServerHost = process.env.CLOUD_SERVER_CONFIG_HOST || 'http://cloud-config-server-pro.2grcvq3qpm.eu-west-1.elasticbeanstalk.com';
    var configServerEndpoint = configServerHost + '/socket-mq-service/';
    var environemnt = process.env.ENVIRONMENT || 'minikube';
    logger.info("Environment : " + environemnt);
    var req = request('GET', configServerEndpoint + environemnt);
    var body = JSON.parse(req.getBody('utf8'));
    var config = body.propertySources[0].source;
    var configParsed = {};
    /* store config in env vars as datasources.json reads from env vars*/
    for (var key in config) {
      if (config.hasOwnProperty(key)) {

        var envVariable = key.replace('.', '_');
        var envVariabeValue = config[key];
        process.env[envVariable] = envVariabeValue;
        configParsed[envVariable] = envVariabeValue;
      }
    }
    logger.info('Remote config : ' + JSON.stringify(configParsed));
    /* set cloud config as an app variable */
    app.set('cloudConfig', configParsed);

  } catch (err) {
    logger.info('Error downloading remote config : ' + err);
  }
}
