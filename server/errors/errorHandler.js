var logger = require("../logger/logger");

initialize();

function initialize() {
  process.on('uncaughtException', function (er) {
    logger.error("uncaughtException (handled by app): " + er.stack) // [3]
    //process.exit(1);
  })
}
