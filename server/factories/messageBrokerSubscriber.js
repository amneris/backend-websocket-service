"use strict";
var logger = require("../logger/logger");
var amqp = require('amqplib/callback_api');
var assert = require('assert')

/**
 * class for subscribing to message broker (listen to events that RabbitMQ receieves on it's queue)
 */
class MessageBrokerSubscriber {

  /**
   * subscribes to message broker service RabbitMQ.
   * have chosen not to monitor this method as connection is only made upon starting of the app
   * @param callback function to call when a new event arrives in the RabbitMQ queue
   */
  subscribe(config, callback, uniqueQueueID) {

    try {

      var amqpUsername = config.amqp_username;
      var amqpPassword = config.amqp_password;
      var userCredentials = '';

      if (amqpUsername != '' && amqpPassword != '') {
        userCredentials = amqpUsername + ':' + amqpPassword + '@';
      }

      var messageQueue = 'amqp://' + userCredentials + config.amqp_host;

      logger.debug('RabbitMQ host and credentials : ' + messageQueue);

      var self = this;
      const websocketExchange = 'websocket-to-mq';
      const deadLettersQueue = 'websocket-to-mq.deadletters';
      const deadLettersRoutingKey = 'deadletters';
      const subsciprtionEventsQueue = 'websocket-to-mq-' + uniqueQueueID;

      amqp.connect(messageQueue, function (err, conn) {
        conn.createChannel(function (err, ch) {
          var exchange = config.amqp_exchange;

          // assert exists or create exhange with name 'subscription-service' (where we listen to events)
          ch.assertExchange(exchange, 'topic', {durable: true});

          // assert exists or create exhange with name 'websocket-to-mq' (to send dead letters to)
          ch.assertExchange(websocketExchange, 'topic', {durable: true});

          // assert exists or create queue 'websocket-to-mq.deadletters' under exchange 'websocket-to-mq'
          ch.assertQueue(deadLettersQueue, {}, function (err, q) {
            ch.bindQueue(q.queue, websocketExchange, deadLettersRoutingKey);
          });

          let allUpTo = false; //If allUpTo is true, all outstanding messages prior to and including
          // the given message shall be considered acknowledged. If false, or omitted, only the message supplied is acknowledged.

          // assert exists or create queue 'websocket-to-mq.deadletters' under exchange 'websocket-to-mq'
          ch.assertQueue(subsciprtionEventsQueue, {durable: true, deadLetterExchange: websocketExchange, deadLetterRoutingKey: deadLettersRoutingKey, autoDelete: true}, function (err, q) {

            const eventTopicPatternToSubscribeTo = config.amqp_topic;

            logger.info('Waiting for events to arrive on RabbitMQ, host:'
              + config.amqp_host + " exchange:" + exchange + " topic: " + eventTopicPatternToSubscribeTo);

            ch.bindQueue(q.queue, exchange, eventTopicPatternToSubscribeTo);

            ch.consume(q.queue, function (msg) {

              try {

                var contentType = msg.properties.contentType;

                logger.info('Message arrived on messaging queue with following fields : ' + JSON.stringify({orginalMessage: msg.fields}));

                if (contentType == 'application/json') {

                  var payload = JSON.parse(msg.content.toString());

                  logger.info('Message payload : ' + JSON.stringify(payload));

                  //append event's routing routing key to payload
                  Object.assign(payload, {"routingKey": msg.fields.routingKey});

                  ch.ack(msg, allUpTo);

                  callback(payload); // execute callback function

                } else {
                  logger.info("Event received but does message does not contain property content_type:application/json");
                  throw Error('event payload is not valid json');
                }

              } catch (err) {
                logger.error("ERROR : " + err);

                let requeue = false //is truthy, the server will try to put the message or messages back on the queue or queues from which they came
                ch.nack(msg, allUpTo, requeue);
              }
            }, {noAck: false});
          });
        });

        conn.on('error', function (err) {
          logger.error("ERROR WITH RABBIT CONNECTION : " + err);
        })

        conn.on("close", function() {
          logger.error("[AMQP] closing, will try reconnecting immediatly");
          self.subscribe(config, callback);
        });      });

    } catch (err) {
      logger.error("ERROR :" + err);
    }
  }
}

module.exports = MessageBrokerSubscriber;
