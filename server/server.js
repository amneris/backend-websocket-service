'use strict';
require('newrelic');
require('./errors/errorHandler');


var logger = require('./logger/logger');
var loopback = require('loopback');
var boot = require('loopback-boot');
var ClientManager = require('./factories/clientManager');
var MessageBrokerSubscriber = require('./factories/messageBrokerSubscriber');
const uuidV4 = require('uuid/v4');
var app = module.exports = loopback();
require('./cloud-config/script')(app); //initialize app config (stored in cloud) before booting socketio


/**
 * function to start web server
 * @returns {http.Server}
 */
app.start = function() {
  return app.listen(function() {
    logger.info('Environment variable APP_NAME : %s', process.env.APP_NAME);
    logger.info('Environment variable NEW_RELIC_LICENSE_KEY : %s', process.env.NEW_RELIC_LICENSE_KEY);
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    logger.info('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      logger.info('Browse the REST API at : %s%s', baseUrl, explorerPath);
      logger.info("Check the application's health status at : %s%s", baseUrl, '/health');
    }
  });
};

/**
 * boots socket IO and start app upon success
 */
function bootSocketIO(app) {
  //create unique id
  const uniqueQueueID = uuidV4();

  var clientManager = new ClientManager(app, uniqueQueueID); // creating an instance of ClientManager initializes socket.io
  var messageBrokerSubscriber = new MessageBrokerSubscriber();
  var callback = clientManager.emitToSocket.bind(clientManager); // define callback for when event is received
  messageBrokerSubscriber.subscribe(app.get('cloudConfig'), callback, uniqueQueueID);
}


// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) {
    bootSocketIO(app);
  }
});
